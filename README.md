# NASAL-Geom

## Software description

NASAL-Geom is a 3D upper respiratory tract geometry reconstruction software developed by [NASAL Systems](www.nasalsystems.com), released as free software under the [Affero-GPL v3 license](https://www.gnu.org/licenses/agpl-3.0.html).

With this software you can load Computerized Tomography (CT) [DICOM files](http://dicom.nema.org/) -A *de facto* standard of the industry- and produce the associated 3D surfaces:

![](examples/imgs/example1.png)

The geometry will include the nasal cavity (solid red), the head (transparent white), and optionally the patchs at the nostrils and pharynx (solid blue). The software may also compute a Rhinometry approximation, that is the area of the coronal slices, conveniently split in left and right cavities.

![](examples/imgs/rhinometry1.png)

If you are publishing an article where NASAL-Geom is applied, please cite the following work:

Cercos-Pita, J.L., Cal, I.R., Duque, D. and Sanjuán de Moreta, G., 2017. NASAL-Geom, a free upper respiratory tract 3D model reconstruction software. Computer Physics Communications.

## Install

NASAL-Geom depends on VTK, and more specdifically, in its Python Wrapper.
Its version 8.0 has been recently included in [PyPI repository](https://pypi.python.org/pypi/vtk).
However, installing it from PyPI may result in some incompatibilities with your already installed VTK libraries.
To avoid such issues, NASAL-Geom is not including it as a dependency for the time being.
Indeed, VTK should be manually installed by the user.
Almost all Linux distributions has a packaged called `python-vtk`.
Alternatively you can rely on the PyPI package:

```
pip install vtk
```

Satisfied the VTK dependency, the easiest way to install NASAL-Geom is using pip package manager:

```
pip install nasalgeom
```

Alternatively, you can download and install the latest development version
typing:

```
git clone https://gitlab.com/sanguinariojoe/nasal-geom.git
cd nasal-geom
python setup.py install
```

## Usage

A wide variety of features has been implemented in NASAL-Geom, that can be optionally combined to produce the final geometry. You can type the following command to learn more about all the possibilities of NASAL-Geom:

```
nasalgeom -h
```

However, for the most common applications the following command would do the job:

```
nasalgeom DICOM_PATH --save-dicom --mar \
          --smooth-factor=0.5 --smooth-median --smooth-gaussian \
          --rhinometry-data --rhinometry-graph \
          --patch-wall --patch-improve-nostrils
```

where `DICOM_PATH` should be replaced by the folder where the DICOM files are placed. You may also provide a VTI file path instead of DICOM folder.

## Documentation

Probably you should start visiting the [wiki page](https://gitlab.com/sanguinariojoe/nasal-geom/wikis/home), where further details regarding the implementation and usage are provided. Regarding the validation of the tool, and the quality of the results, they are discussed in the main paper:

Cercos-Pita, J.L., Cal, I.R., Duque, D. and Sanjuán de Moreta, G., 2017. NASAL-Geom, a free upper respiratory tract 3D model reconstruction software. Computer Physics Communications.
